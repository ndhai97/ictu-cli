import xlrd
import os
import gcalendar
from datetime import date, timedelta, datetime


MORNING_SESSION   = range(1,6)
AFTERNOON_SESSION = range(6,13)


def sanity_weekday(given_weekday):
    return {
        'T2': 0,
        'T3': 1,
        'T4': 2,
        'T5': 3,
        'T6': 4,
        'T7': 5,
        'CN': 6,
    }[given_weekday]

def sanity_course_type(given_type):
    return {
        'TL': '[Thảo Luận]',
        'LT': '[Lý Thuyết]',
        'TH': '[Thực Hành]',
    }[given_type]

def sanity_course_name(given_name):
    redundant_text = given_name.find("-")
    return given_name[:redundant_text]

def is_summer(given_date):
    current_year = datetime.now().year
    summer_start = date(current_year, 4, 15)
    summer_end   = date(current_year, 10, 14)

    delta = summer_end - summer_start

    for i in range(delta.days + 1):
        summer_date = summer_start + timedelta(i)
        if given_date == summer_date:
            return True

    return False

def sanity_date(given_date):
    day, month, year = given_date.split("/")
    # Using int() to convert day or month like 02 to 2
    return date(int(year), int(month), int(day))

def sanity_study_time(study_session, given_date):
    if study_session in MORNING_SESSION:
        if is_summer(given_date):
            return {
                1: '06:30:00',
                2: '07:25:00',
                3: '08:25:00',
                4: '09:25:00',
                5: '10:20:00',
            }[study_session]
        else:
            return {
                1: '06:45:00',
                2: '07:40:00',
                3: '08:40:00',
                4: '09:40:00',
                5: '10:35:00',
            }[study_session]
    elif study_session in AFTERNOON_SESSION:
        return {
                6: '13:00:00',
                7: '13:55:00',
                8: '14:55:00',
                9: '15:55:00',
                10: '16:50:00',
                11: '18:15:00',
                12: '19:10:00',
        }[study_session]
    else:
        return False

def sanity_rest_time(study_session, given_date):
    if study_session in MORNING_SESSION:
        if is_summer(given_date):
            return {
                1: '07:20:00',
                2: '08:15:00',
                3: '09:15:00',
                4: '10:15:00',
                5: '11:10:00',
            }[study_session]
        else:
            return {
                1: '07:35:00',
                2: '08:30:00',
                3: '09:30:00',
                4: '10:30:00',
                5: '11:25:00',
            }[study_session]
    elif study_session in AFTERNOON_SESSION:
        return {
            6: '13:50:00',
            7: '14:45:00',
            8: '15:45:00',
            9: '16:45:00',
            10: '17:40:00',
            11: '19:05:00',
            12: '20:00:00',
        }[study_session]
    else:
        return False

def sanity_time_range(given_range):
    course_start_date, course_end_date = given_range.split("->")
    return [sanity_date(course_start_date), sanity_date(course_end_date)]

def get_study_date(given_range, given_weekday):
    start_study, end_study = sanity_time_range(given_range)
    delta = end_study - start_study
    study_date = []

    for i in range(delta.days + 1):
        date_in_period = start_study + timedelta(i)
        if date_in_period.weekday() == sanity_weekday(given_weekday):
            study_date.append(date_in_period)
    return study_date

def extract_worksheet_data(worksheet):
    sheet_data = []
    recent_day = ''

    for row in range(6, worksheet.nrows):
        temp = []

        # This fix the empty weekday cell with data from the previous one
        #
        # Ex: if the previous weekday cell is 'T2' and the cell after that
        # is empty, this will fill that empty cell with 'T2'
        #
        if worksheet.cell_value(row, 0) == '':
            temp.append(recent_day)
        else:
            recent_day = worksheet.cell_value(row, 0)

        for col in range(0, worksheet.ncols):
            if worksheet.cell_value(row, col) != '':
                temp.append(worksheet.cell_value(row, col))

        sheet_data.append(temp)

    return sheet_data

def process_xls(path_to_xls):
    workbook = xlrd.open_workbook(
        path_to_xls,
        logfile=open(os.devnull, 'w'),
        encoding_override="UTF-8"
        )
    worksheet = workbook.sheet_by_index(0)

    for row in extract_worksheet_data(worksheet):
        # Fix for course that don't have teacher name
        if len(row) == 7:
            weekday, study_session, period_time, course_name, course_type, location, teacher_name = row
        else:
            weekday, study_session, period_time, course_name, course_type, location = row
            teacher_name = "Không xác định"

        for study_date in get_study_date(period_time, weekday):
            start_study_session, end_study_session = study_session.split("->")

            start_time = sanity_study_time(int(start_study_session), study_date)
            end_time   = sanity_rest_time(int(end_study_session), study_date)

            description = ('{0}{n}Tên môn học: {1}{n}Tiết học: {2}{n}Tên giảng viên: {3}').format(
                sanity_course_type(course_type),
                sanity_course_name(course_name),
                study_session,
                teacher_name,
                n = '\n'
                )

            gcalendar.create_event(
                title = sanity_course_name(course_name),
                location = location,
                description = description,
                start_time = str(study_date) + "T" + str(start_time),
                end_time = str(study_date) + "T" + str(end_time)
                )
