import os
import re
import xls
import gcalendar
import hashlib
import codecs
import requests
import logging
from datetime import date, datetime
from bs4 import BeautifulSoup


session_url = ''
current_semester = ''
downloaded_schedules = []
header_params = {}
SESSION_REQUEST = requests.Session()
logging.basicConfig(filename='debug.log', level=logging.INFO)


def md5(plain_text):
    return hashlib.md5(plain_text.encode("UTF-8")).hexdigest()


def send_request(*, type, target, **kwargs):
    global header_params
    no_soup = kwargs.get('no_soup', False)
    no_header = kwargs.get('no_header', False)
    URL = session_url + target
    logging.info('current url: ' + URL)

    if type == 'get':
        response = SESSION_REQUEST.get(URL)
    elif type == 'post':
        response = SESSION_REQUEST.post(url=URL, data=header_params)
    else:
        return False

    if not no_header:
        make_headers(response.text)
    logging.info('status:' + str(response.status_code))
    if no_soup:
        return response.content
    return BeautifulSoup(response.text, 'html.parser')


def make_headers(response):
    global header_params
    soup = BeautifulSoup(response, 'html.parser')
    header_params = {}
    header_params.update({
        '__EVENTTARGET':  '',
        '__EVENTARGUMENT': '',
        '__LASTFOCUS':    '',
        'PageHeader1$drpNgonNgu': '010527EFBEB84BCA8919321CFD5C3A34'
    })
    header_params.update({
        e['name']: e.get('value')
        for e in soup.find_all('input', {'id': True})
    })


def is_connectable():
    global session_url
    URL = 'http://dangkytinchi.ictu.edu.vn/kcntt/Login.aspx'
    response = SESSION_REQUEST.get(URL)
    if response.status_code == requests.codes.ok:
        session_url = response.url[:-11]
        make_headers(response.content)
        return True
    return False


def login(username, password):
    if is_connectable():
        global header_params
        header_params.update({
            'txtUserName': username,
            'txtPassword': md5(password)
        })
        send_request(
            type = 'post',
            target = '/Login.aspx?url=http://dangkytinchi.ictu.edu.vn/kcntt/Home.aspx',
            no_header = True
            )


def get_current_semester():
    response = send_request(
        type = 'get',
        target = '/StudyRegister/StudyRegister.aspx',
        no_header = True
        )
    title = response.find('span', {'id': 'lblTitle'}).text
    semester = title[45:46]
    school_year = title[55:64]

    return (f'{semester}_{school_year}')


def sanity_exam_time(time):
    start_pos = time.find('(')
    end_pos = time.find(')')
    return time[start_pos+1:end_pos].split('-')


def sanity_exam_date(given_date):
    day, month, year = given_date.split('/')
    return date(int(year), int(month), int(day))


def query_examination_number(number, semester):
    header_params.update({
        '__EVENTTARGET': 'drpSemester',
        'drpSemester': semester,
        'drpExaminationNumber': number,
        'drpDotThi': ''
    })
    response = send_request(type='post', target='/StudentViewExamList.aspx')
    for i in response.select('#drpDotThi option'):
        if i['value'] != '':
            print('đợt thi: ' + i.text)
            query_examinations(number, semester, i['value'])

def query_examinations(number, semester, dot_thi):
    global header_params
    header_params.update({
        '__EVENTTARGET': 'drpDotThi',
        'drpSemester': semester,
        'drpExaminationNumber': number,
        'drpDotThi': dot_thi
    })
    response = send_request(type='post', target='/StudentViewExamList.aspx')
    table = response.find('table', {'id': 'tblCourseList'})
    data = []
    for row in table.find_all('tr'):
        temp = []
        for col in row.find_all('td'):
            #using regexp to remove \t \r \n
            temp.append(re.sub(r'[\n\r\t]', '', col.text))
        data.append(temp)

    # remove first and last row
    data = data[1:len(data) - 1]
    #print("num of row: " + str(len(row)))
    for row in data:
        row = row[1:len(row) -1]
        course_id, course_name, course_credit, exam_date, exam_time, exam_type, candidate_number, location = row
        begin_time, end_time = sanity_exam_time(exam_time)
        start_time =  str(sanity_exam_date(exam_date)) + "T" + begin_time + ':00'
        end_time   =  str(sanity_exam_date(exam_date)) + "T" + end_time + ':00'
        title = "[THI] " + course_name
        description = ("Tên môn: {0}Số tín chỉ: {1}Hình thức thi: {2}Số báo danh: {3}Ca thi: {4}").format(
            course_name + '\n',
            course_credit + '\n',
            exam_type + '\n',
            candidate_number + '\n',
            exam_time + '\n'
            )
        print(title)
        gcalendar.create_event(
            title = title,
            location = location,
            description = description,
            start_time = start_time,
            end_time = end_time
            )


def get_exam():
    global header_params
    response = send_request(
        type = 'get',
        target = '/StudentViewExamList.aspx'
        )
    current_semester = get_current_semester()
    for semester in response.select('#drpSemester option'):
        if semester.text == current_semester:
            print('học kỳ hiện tại: ' + semester.text)
            for i in range(0,2):
                query_examination_number(i, semester['value'])


def get_news_id():
    response = send_request(
        type = 'get',
        target = '/Home.aspx'
        )
    important_news = response.find('div', {'class': 'important_news'})
    news_id = []
    for new_id in important_news.select('a'):
        news_id.append(new_id['href'])

    return news_id


def get_public_date(given_title):
    date_start = given_title.rfind('(') + 1
    date_end = given_title.rfind(')')

    day, month, year = given_title[date_start:date_end].split('/')
    return date(int(year), int(month), int(day))


def sanity_new_title(given_title):
    title_end = given_title.rfind('(') - 1
    return given_title[:title_end]


def sync_news():
    for new_id in get_news_id():
        response = send_request(
            type = 'get',
            target = '/' + new_id
            )
        new_title = response.find(id="lblTieude").get_text()
        new_content = response.find(id="lblChitiet").get_text("\n")
        gcalendar.create_event(
            title = sanity_new_title(new_title),
            location = "ictu-news",
            description = new_content,
            start_time = str(get_public_date(new_title)) + "T07:25:00",
            end_time = str(get_public_date(new_title)) + "T19:00:00"
        )


def query_study_schedule():
    response = send_request(
        type = 'get',
        target = '/Reports/Form/StudentTimeTable.aspx'
        )
    for term in response.select('#drpTerm > option'):
        query_study_schedule_by_term(term['value'])


def query_study_schedule_by_term(term):
    global header_params
    header_params.update({
        'drpTerm': term,
        'drpType': 'E'
    })

    student_time_table = send_request(
        type = 'post',
        target = '/Reports/Form/StudentTimeTable.aspx',
        no_soup = True
    )
    download_study_schedule(term, student_time_table)


def download_study_schedule(study_term, new_student_time_table):
    path = str(os.getcwd()) + '/ThoiKhoaBieuHocKy' + str(study_term) + '.xls'
    downloaded_schedules.append(path)

    time_table = codecs.open(path, "w", "latin1")
    time_table.write(new_student_time_table.decode("latin1"))
    time_table.close()


def query_unfinished_courses():
    global header_params
    header_params.update({
        '__EVENTTARGET': 'drpFilter',
        'drpFilter': 2
    })
    response = send_request(type='post', target='/StudentMark.aspx')
    table = response.find('table', {'id': 'tblStudentMark'})
    data = []
    for row in table.find_all('tr'):
        temp = []
        for col in row.find_all('td'):
            #using regexp to remove \t \r \n
            temp.append(re.sub(r'[\n\r\t]', '', col.text))
        data.append(temp)

    # remove first and last row
    data = data[1:len(data) - 1]
    for row in data:
        course_name = row[2]
        scores = row[13]
        print('• ' + course_name + ', ' + scores)
    
    print('--------------------------------------')
    print('⇒ Chưa đạt: ' + str(len(data)) + ' môn')


def get_student_mark():
    global header_params
    response = send_request(
        type = 'get',
        target = '/StudentMark.aspx'
        )
    query_unfinished_courses()


def sync_study_schedule():
    gcalendar.delete_calendar()
    gcalendar.create_calendar()
    query_study_schedule()

    for each_file in downloaded_schedules:
      xls.process_xls(each_file)

    sync_news()

    get_exam()
