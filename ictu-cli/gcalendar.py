import os
from oauth2client import client
from googleapiclient import sample_tools


PATH = str(os.getcwd()) + '/calendar_id.dat'
CALENDAR_TITLE = 'Thời khóa biểu'
TIMEZONE = 'Asia/Ho_Chi_Minh'
service, flags = sample_tools.init(
    '', 'calendar', 'v3', __doc__, __file__,
    scope='https://www.googleapis.com/auth/calendar')


def create_calendar():
    if not os.path.isfile(PATH):
        calendar = {
            'summary': CALENDAR_TITLE,
            'timeZone': TIMEZONE,
            }

        created_calendar = service.calendars().insert(body=calendar).execute()


def create_event(
        *, title, location, description,
        start_time, end_time):
    event = {
        'summary': title,
        'location': location,
        'description': description,
        'colorId': '10',
        'start': {
            'dateTime': start_time,
            'timeZone': TIMEZONE
        },
        'end': {
            'dateTime': end_time,
            'timeZone': TIMEZONE
        },
        'reminders': {
            'useDefault': False,
            'overrides': [
                { 'method': 'popup', 'minutes': 10 },
            ]
        }
    }

    event = service.events().insert(
        calendarId=current_calendar_id(), body=event
        ).execute()


def delete_calendar():
    page_token = None
    while True:
        calendar_list = service.calendarList().list(pageToken=page_token).execute()
        for calendar_list_entry in calendar_list['items']:
            if calendar_list_entry['summary'] == CALENDAR_TITLE:
                service.calendarList().delete(calendarId=current_calendar_id()).execute()
        page_token = calendar_list.get('nextPageToken')
        if not page_token:
            break


def current_calendar_id():
    calendar_list = service.calendarList().list().execute()
    for calendar_list_entry in calendar_list['items']:
        if calendar_list_entry['summary'] == CALENDAR_TITLE:
            return calendar_list_entry['id']
