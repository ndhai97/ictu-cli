# ICTU-cli

A python script that allow you to download schedules, news and even score of course that you not pass yet. All via terminal

# Main Feature

- Download all schedules, parse the data and upload it to Google calendar
- View which Course that you hasn't pass yet (which really handy for me)
- Add newest notifications to Google calendar
- Add exam schedules to Google calendar

As of why I use Google calendar, it's because once the event are on Google calendar other calendar can sync that event as well, hence you can see your calendar on almost every devices (smart-phone, PC, ...).

# Usage

```bash
git clone https://gitlab.com/ndhai97/ictu-cli
pip install -r requirements.txt
# To download scheduals run this
python3 ictu-cli/ -s
# To view which course you hasn't pass yet and it score run this
python3 ictu-cli/ -m
```

**NOTICE:** Before running this script, open ictu-cli/\_\_main\_\_.py and change USERNAME and PASSWORD accordingly. Also when you run this for the first time, it will ask you to login to your Google account so that you could give this script permission to create calendars and write events, this is normal behavior and not thing to be worry of.